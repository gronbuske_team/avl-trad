#include "MyAVLTree.h"
#include <iostream>
#include <algorithm>



MyAVLTree::MyAVLTree()
{
	root = nullptr;
}


MyAVLTree::~MyAVLTree()
{
	delete root;
}

void MyAVLTree::Insert(int i)
{
	if (root == nullptr)
	{
		root = new Node(i);
		return;
	}
	InsertRec(root, i);
}

Node* MyAVLTree::Remove(int i)
{
	return DeleteRec(root, i);
}

Node* MyAVLTree::MinElement()
{
	Node* current = root;
	while (current->leftNode != nullptr)
		current = current->leftNode;
	return current;
}

bool MyAVLTree::Empty()
{
	return root == nullptr;
}

void MyAVLTree::Print()
{
	PrintRec(root);
}

void MyAVLTree::PrintRec(Node* currentNode)
{
	if (currentNode == nullptr)
		return;
	PrintRec(currentNode->leftNode);
	std::cout << currentNode->content << '\n';
	PrintRec(currentNode->rightNode);
}

Node* MyAVLTree::InsertRec(Node* current, int value)
{
	if (current == nullptr)
	{
		current = new Node(value);
		return current;
	}
	if (value < current->content)
		current->leftNode = InsertRec(current->leftNode, value);
	else if (value > current->content)
		current->rightNode = InsertRec(current->rightNode, value);
	else
		return current;

	int balance = GetBalance(current);

	if (balance < -1)
	{
		if (value > current->rightNode->content)
			return LeftRotate(current);
		else if (value < current->rightNode->content)
		{
			current->rightNode = RightRotate(current->rightNode);
			return LeftRotate(current);
		}
	}
	else if (balance > 1)
	{
		if (value < current->leftNode->content)
			return RightRotate(current);
		else if (value > current->leftNode->content)
		{
			current->leftNode = LeftRotate(current->leftNode);
			return RightRotate(current);
		}
	}

	return current;
}

Node* MyAVLTree::DeleteRec(Node* current, int value)
{
	if (current == nullptr)
		return current;

	if (value < current->content)
		current->leftNode = DeleteRec(current->leftNode, value);
	else if (value > current->content)
		current->rightNode = DeleteRec(current->rightNode, value);
	else
	{
		if (current->leftNode == nullptr)
		{
			if (current->rightNode == nullptr)
				current = nullptr;
			else
				*current = *current->rightNode;
		}
		else if (current->rightNode == nullptr)
			*current = *current->leftNode;
		else
		{
			Node* temp = current->rightNode;
			while (temp->leftNode != nullptr)
				temp = temp->leftNode;
			current->content = temp->content;
			current->rightNode = DeleteRec(current->rightNode, temp->content);
		}
	}
	if (current == nullptr)
		return current;

	int balance = GetBalance(current);
	if (balance < -1)
	{
		if (GetBalance(current->rightNode) <= 0)
			return LeftRotate(current);
		else
		{
			current->rightNode = RightRotate(current->rightNode);
			return LeftRotate(current);
		}
	}
	else if (balance > 1)
	{
		if (GetBalance(current->leftNode) >= 0)
			return RightRotate(current);
		else
		{
			current->leftNode = LeftRotate(current->leftNode);
			return RightRotate(current);
		}
	}

	return current;
}

int MyAVLTree::GetBalance(Node* node)
{
	if (node == nullptr)
		return 0;
	return GetHeight(node->leftNode) - GetHeight(node->rightNode);
}

int MyAVLTree::GetHeight(Node* node)
{
	if (node == nullptr)
		return 0;
	return node->height;
}

Node* MyAVLTree::RightRotate(Node* node)
{
	Node* x = node->leftNode;
	Node* T2 = x->rightNode;

	x->rightNode = node;
	node->leftNode = T2;

	node->height = std::max(GetHeight(node->leftNode), GetHeight(node->rightNode));
	x->height = std::max(GetHeight(x->leftNode), GetHeight(x->rightNode));

	return x;
}

Node* MyAVLTree::LeftRotate(Node* node)
{
	Node* y = node->leftNode;
	Node* T2 = node->rightNode;

	y->leftNode = node;
	node->rightNode = T2;

	node->height = std::max(GetHeight(node->leftNode), GetHeight(node->rightNode));
	y->height = std::max(GetHeight(y->leftNode), GetHeight(y->rightNode));

	return y;
}
