#pragma once
class Node
{
public:
	Node();
	Node(int content);
	~Node();

	int content;
	int height;
	Node* leftNode;
	Node* rightNode;
};

