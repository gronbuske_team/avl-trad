#include "Node.h"



Node::Node()
{
	leftNode = nullptr;
	rightNode = nullptr;

	content = 0;
}

Node::Node(int content)
{
	this->content = content;
	this->height = 1;
}

Node::~Node()
{
	delete leftNode, rightNode;
}
