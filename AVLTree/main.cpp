#include "MyAVLTree.h"
#include <random>
#include <ctime>
#include <iostream>



void main()
{
	/*
	Minsta maxh�jd f�r AVL-tr�det �r 7 f�r 100 noder, om man fyller ett bin�rt tr�d till max kr�vs k-niv�er d�r 2^k>antal noder.
	2^7=128 och 2^6=64: d�rf�r kr�vs 7 niv�er minst.

	H�gsta maxh�jd f�r AVL-tr�det 9 f�r 100 noder, minsta antalet noder f�r att f� en niv� h�gre som max f�ljer Fibonacci's talsumma:
	1 2 4 7 12 20 33 54 88 133 osv
	D�rf�r beh�vs det 133 noder f�r att kunna ha ett AVL-tr�d med 10 i djup, och det maximala djupet med 100 noder �r d� 9.

	Ifall ett Splay-tr�d hade anv�nts ist�llet hade tr�det byggts upp p� ett trolligtvis ok s�tt och om inte slumpningen �r konstig 
	borde tr�det bli n�gorlunda balanserat. Under borttagningen flyttas d�remot noden som ska tas bort till toppen av tr�det och 
	tr�det blir mindre j�mnt f�r varje borttagning. Splaytr�det kommer bli on�digt djupt.
	*/

	std::srand(std::time(0));
	MyAVLTree myTree = MyAVLTree();
	for (int i = 0; i < 100; i++)
		myTree.Insert(std::rand() % 1000 + 1);
	myTree.Print();
	for (int i = 0; i < 40; i++)
		myTree.Remove(myTree.MinElement()->content);
	myTree.Print();
	std::cout << "Write q to exit\n";
	char input = ' ';
	while (input != 'q')
		std::cin >> input;
}