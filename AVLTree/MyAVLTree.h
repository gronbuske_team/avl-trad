#pragma once
#include "Node.h"

class MyAVLTree
{
public:
	MyAVLTree();
	~MyAVLTree();

	void Insert(int i);
	Node* Remove(int i);
	Node* MinElement();
	bool Empty();
	void Print();

private:
	void PrintRec(Node* currentNode);
	Node* InsertRec(Node* current, int value);
	Node* DeleteRec(Node* current, int value);
	int GetBalance(Node* node);
	int GetHeight(Node* node);
	Node* RightRotate(Node* node);
	Node* LeftRotate(Node* node);
	Node* root;
};

